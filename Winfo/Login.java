package com.example.winfo;

import java.util.ArrayList;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

@SuppressLint("ShowToast")
public class Login extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.login, menu);
		return true;
	}

	public void login(View view) {
		// Logindaten holen
		EditText editText_email = (EditText) findViewById(R.id.editText_email);
		String email = editText_email.getText().toString();
		EditText editText_password = (EditText) findViewById(R.id.editText_password);
		String password = editText_password.getText().toString();

		LoginVerification login = new LoginVerification();
		login.execute(email, password);

	}

	public class LoginVerification extends AsyncTask<String, Void, Boolean> {

		@Override
		protected Boolean doInBackground(String... params) {

			Boolean error = null;
			String apiKey = null;
			Boolean loginSuccessful = false;
			String email = params[0];
			String password = params[1];

			ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
			nameValuePairs.add(new BasicNameValuePair("email", email));
			nameValuePairs.add(new BasicNameValuePair("password", password));

			// Create the HTTP request
			HttpParams httpParameters = new BasicHttpParams();

			// Setup timeouts
			HttpConnectionParams.setConnectionTimeout(httpParameters, 15000);
			HttpConnectionParams.setSoTimeout(httpParameters, 15000);

			HttpClient httpclient = new DefaultHttpClient(httpParameters);
			HttpPost httppost = new HttpPost(
					"http://pc-androidhft.rhcloud.com/v1/login");
			try {
				Log.d("MyApp", "try");
				HttpResponse response;
				httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
				response = httpclient.execute(httppost);
				Log.d("MyApp", "response");

				Log.d("MyApp", "httppost");
				HttpEntity entity = response.getEntity();
				Log.d("MyApp", "httpentity");
				String result = null;
				result = EntityUtils.toString(entity);
				Log.d("MyApp", "result");
				// Create a JSON object from the request response
				JSONObject jsonObject = new JSONObject(result);

				// Retrieve the data from the JSON object
				error = jsonObject.getBoolean("error");
				apiKey = jsonObject.getString("apiKey");
				Log.d("MyApp", apiKey);
				Log.d("MyApp", "Nach json");

			} catch (Exception e) {
				Log.d("MyApp", "Exception");
				e.printStackTrace();
			}

			if (error == false) {
				loginSuccessful = true;
			}
			return loginSuccessful;

		}

		protected void onPostExecute(Boolean loginSuccessful) {
			if (loginSuccessful) {
				Intent intent = new Intent(Login.this, MainActivity.class);
				startActivity(intent);
			} else {
				Context context = getApplicationContext();
				Toast.makeText(context, context.getString(R.string.loginError),
						Toast.LENGTH_LONG).show();
			}

		}

	}

}
