package com.example.winfo;

import java.util.ArrayList;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

import android.os.AsyncTask;
import android.util.SparseBooleanArray;

public class DeleteEntry extends AsyncTask<String, Void, Boolean> {

	private Boolean deleted = false;
	private SparseBooleanArray selected;
	
	

	public void setSelected(SparseBooleanArray selected) {
		this.selected = selected;
	}

	@Override
	protected Boolean doInBackground(String... params) {
		String apiKey = params[0];
		String mainDataIDS = params[1];
		
		ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
		nameValuePairs.add(new BasicNameValuePair("ids", mainDataIDS));
		
		try {
			
			HttpParams httpParams = new BasicHttpParams();
			
			HttpConnectionParams.setConnectionTimeout(httpParams, 15000);
			HttpConnectionParams.setSoTimeout(httpParams, 15000);
			
			HttpClient client = new DefaultHttpClient(httpParams);
			HttpPost httppost = new HttpPost(
					"http://winfo-androidhft.rhcloud.com/v1/deletemultidata");


			httppost.setHeader("Authorization", apiKey);
			
			httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
			HttpResponse response = client.execute(httppost);
			HttpEntity entity = response.getEntity();
			String result = null;
			result = EntityUtils.toString(entity);
			// Create a JSON object from the request response
			// Retrieve the data from the JSON object
			JSONObject obj = new JSONObject(result);

			String error = obj.getString("error");
			if (error == "false") {
				deleted = true;
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return deleted;
	}

	@Override
	protected void onPostExecute(Boolean deleted) {
		mListener.callbackDeleteMainData(deleted);
	}

	CallBackListener mListener;

	public void setListener(CallBackListener listener) {
		mListener = listener;
	}
}
