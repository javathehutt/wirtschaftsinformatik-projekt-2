package com.example.winfo;

public interface CallBackListener {

	public void callbackNewMainData(boolean inserted);

	public void callbackDeleteMainData(boolean deleted);

	public void callbackUpdatedMainData(boolean updated);

}
