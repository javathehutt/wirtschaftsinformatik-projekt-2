package com.example.winfo;

import java.util.ArrayList;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import android.app.Dialog;
import android.os.AsyncTask;


	public class CreateMainData extends AsyncTask<String, Void, Boolean> {

		private ArrayList<mainData> mainData = new ArrayList<mainData>();
		boolean inserted = false;
		String standardURL = "http://winfo-androidhft.rhcloud.com/v1/createMainData";
		String commentURL = "http://winfo-androidhft.rhcloud.com/v1/createMainAdditional";

		@Override
		protected Boolean doInBackground(String... params) {

			String apiKey = params[0];
			String startTime = params[1];
			String endTime = params[2];
			String pause = params[3];
			String activity= params[4];
			String comment = params[5];
			String newComment = params[6];
			
			ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
			nameValuePairs.add(new BasicNameValuePair("startTime", startTime));
			nameValuePairs.add(new BasicNameValuePair("endTime", endTime));
			nameValuePairs.add(new BasicNameValuePair("pause", pause));
			nameValuePairs.add(new BasicNameValuePair("activity", activity));
			
			if (newComment.equals("true")){
				nameValuePairs.add(new BasicNameValuePair("comment", comment));
			}
			
			HttpParams httpParams = new BasicHttpParams();
			
			HttpConnectionParams.setConnectionTimeout(httpParams, 15000);
			HttpConnectionParams.setSoTimeout(httpParams, 15000);
			
			
			HttpClient httpclient = new DefaultHttpClient(httpParams);
			HttpPost httppost = null;
			if (newComment.equals("true")){
				httppost = new HttpPost(
						commentURL);
			} else{
				httppost = new HttpPost(
						standardURL);
			}


			try {
				HttpResponse response;
				httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
				httppost.setHeader("Authorization", apiKey);
				response = httpclient.execute(httppost);

				HttpEntity entity = response.getEntity();

				String result = null;
				result = EntityUtils.toString(entity);
				// Create a JSON object from the request response
				// Retrieve the data from the JSON object
				JSONObject jObj = new JSONObject(result);
				
				boolean error = jObj.getBoolean("error");
				if (error == false){
					inserted = true;
				}
				
				
			} catch (Exception e) {
				e.printStackTrace();
			}

			return inserted;

		}
		@Override
		protected void onPostExecute(Boolean inserted) {
			mListener.callbackNewMainData(inserted);
		}

		CallBackListener mListener;
		
		public void setListener(CallBackListener listener){
			mListener = listener;
		}
		
	}

