package com.example.winfo;

import java.util.ArrayList;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.ActionMode;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

public class EditFragment extends Fragment implements CallBackListener {

	View rootView = null;
	ListView list;
	SharedPreferences sharedpreferences;
	public static String apiKey;
	public static final String UserPREFERENCES = "UserPrefs";
	ArrayList<mainData> deleteList;
	MenuItem action_add;
	MenuItem action_logout;
	MenuItem action_refresh;
	ProgressBar progress;
	SparseBooleanArray selected;
	CustomListAdapter adapter;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		rootView = inflater.inflate(R.layout.fragment_edit, container, false);

		sharedpreferences = getActivity().getSharedPreferences(UserPREFERENCES,
				Context.MODE_PRIVATE);
		apiKey = sharedpreferences.getString("apiKey", "");

		deleteList = new ArrayList<mainData>();
		progress = (ProgressBar) rootView.findViewById(R.id.progressBar);
		progress.setVisibility(View.VISIBLE);
		FetchData fetch = new FetchData();
		fetch.execute(apiKey);
		Log.d("myApp", "mainData");
		setHasOptionsMenu(true);
		return rootView;

	}

	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		inflater.inflate(R.menu.main, menu);

		action_add = menu.findItem(R.id.action_add);
		action_logout = menu.findItem(R.id.action_logout);
		action_refresh = menu.findItem(R.id.action_refresh);

		super.onCreateOptionsMenu(menu, inflater);
	}

	public void logout() {
		sharedpreferences = getActivity().getSharedPreferences(UserPREFERENCES,
				Context.MODE_PRIVATE);
		Editor editor = sharedpreferences.edit();
		editor.clear();
		editor.commit();

		Intent intent = new Intent(getActivity(), Login.class);
		intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		startActivity(intent);
	}

	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		if (id == R.id.action_logout) {
			logout();
			return true;
		}
		if (id == R.id.action_add) {

			Intent intent = new Intent(getActivity().getApplicationContext(),
					ListViewItemDetails.class);
			intent.putExtra("currentView", "add");
			startActivity(intent);
		}
		if (id == R.id.action_refresh) {
			progress.setVisibility(View.VISIBLE);
			list.setVisibility(View.GONE);
			FetchData fetch = new FetchData();
			fetch.execute(apiKey);
		}
		return super.onOptionsItemSelected(item);
	}

	public class FetchData extends AsyncTask<String, Void, ArrayList<mainData>> {

		private ArrayList<mainData> mainData = new ArrayList<mainData>();

		@Override
		protected ArrayList<mainData> doInBackground(String... params) {
			String apiKey = params[0];

			HttpClient httpclient = new DefaultHttpClient();
			HttpGet httpget = new HttpGet(
					"http://winfo-androidhft.rhcloud.com/v1/data");

			try {
				HttpResponse response;
				httpget.setHeader("Authorization", apiKey);
				response = httpclient.execute(httpget);

				HttpEntity entity = response.getEntity();

				String result = null;
				result = EntityUtils.toString(entity);
				// Create a JSON object from the request response
				// Retrieve the data from the JSON object
				JSONObject jObj = new JSONObject(result);
				JSONArray jArr = jObj.getJSONArray("data");
				for (int i = 0; i < jArr.length(); i++) {
					JSONObject obj = jArr.getJSONObject(i);
					mainData.add(new mainData(obj.getInt("idmainData"), obj
							.getString("startTime"), obj.getString("endTime"),
							obj.getString("pause"), new MyActivity(obj
									.getInt("activity"), obj
									.getString("activityHeader"), obj
									.getString("activityDescription"),
									new MyEntity(obj.getInt("entity"), obj
											.getString("entityName"))),
							new AdditionalData(obj.optInt("additionalData"),
									obj.optString("comment", ""))));

				}
			} catch (Exception e) {
				e.printStackTrace();
			}

			return mainData;

		}

		protected void onPostExecute(final ArrayList<mainData> mainData) {
			progress.setVisibility(View.GONE);
			adapter = new CustomListAdapter(getActivity()
					.getApplicationContext(), mainData);

			list = (ListView) rootView.findViewById(R.id.activitylist);
			list.setVisibility(View.VISIBLE);

			list.setAdapter(adapter);

			list.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE_MODAL);
			list.setMultiChoiceModeListener(new AbsListView.MultiChoiceModeListener() {

				@Override
				public boolean onActionItemClicked(ActionMode mode,
						MenuItem item) {
					switch (item.getItemId()) {
					case R.id.action_delete:

						new AlertDialog.Builder(getActivity())
								.setTitle("Eintr�ge l�schen")
								.setMessage(R.string.deleteMessage)
								.setPositiveButton(android.R.string.yes,
										new DialogInterface.OnClickListener() {
											public void onClick(
													DialogInterface dialog,
													int which) {

												// Calls getSelectedIds method
												// from ListViewAdapter
												// Class
												selected = adapter
														.getSelectedIds();
												// Captures all selected ids
												// with a loop
												DeleteEntry deleteEntry = new DeleteEntry();
												deleteEntry
														.setListener(EditFragment.this);

												String mainDataIDS = "";
												for (int i = (selected.size() - 1); i >= 0; i--) {
													if (selected.valueAt(i)) {
														mainData selecteditem = adapter
																.getItem(selected
																		.keyAt(i));

														if (!mainDataIDS
																.isEmpty()) {
															mainDataIDS = mainDataIDS
																	+ ",";
														}
														mainDataIDS = mainDataIDS
																+ String.valueOf(selecteditem
																		.getId());
													}
												}

												deleteEntry.execute(apiKey,
														mainDataIDS);
												adapter.removeSelection();
											}
										})
								.setNegativeButton(android.R.string.no,
										new DialogInterface.OnClickListener() {
											public void onClick(
													DialogInterface dialog,
													int which) {
												adapter.removeSelection();
											}
										})
								.show();

						// Close CAB
						mode.finish();
						return true;
					default:
						return false;
					}
				}

				@Override
				public void onItemCheckedStateChanged(ActionMode mode,
						int position, long id, boolean checked) {
					// Capture total checked items
					final int checkedCount = list.getCheckedItemCount();
					// Set the CAB title according to total checked items
					mode.setTitle(checkedCount + " ausgew�hlt");
					// Calls toggleSelection method from ListViewAdapter Class
					adapter.toggleSelection(position);
				}

				@Override
				public boolean onCreateActionMode(ActionMode mode, Menu menu) {
					// TODO Auto-generated method stub
					mode.getMenuInflater().inflate(R.menu.cab_main, menu);
					return true;
				}

				@Override
				public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
					// TODO Auto-generated method stub
					return false;
				}

				@Override
				public void onDestroyActionMode(ActionMode mode) {
					// TODO Auto-generated method stub

				}
			});

			list.setOnItemClickListener(new OnItemClickListener() {
				@Override
				public void onItemClick(AdapterView<?> arg0, View arg1,
						int arg2, long arg3) {
					Intent intent = new Intent();
					intent.setClassName(getActivity().getPackageName(),
							getActivity().getPackageName()
									+ ".ListViewItemDetails");

					intent.putExtra("idmainData", mainData.get(arg2).getId());
					intent.putExtra("startTime", mainData.get(arg2)
							.getStartTime());
					intent.putExtra("endTime", mainData.get(arg2).getEndTime());
					intent.putExtra("pause", mainData.get(arg2).getPause());
					intent.putExtra(
							"activityId",
							String.valueOf(mainData.get(arg2).getActivity()
									.getId()));
					intent.putExtra("activityHeader", mainData.get(arg2)
							.getActivity().getActivityHeader());
					intent.putExtra("activityDesc", mainData.get(arg2)
							.getActivity().getActivityDescription());
					intent.putExtra("entity", mainData.get(arg2).getActivity()
							.getEntity().getName());
					intent.putExtra("additionalData", mainData.get(arg2)
							.getAdditionalData().getId());
					intent.putExtra("comment", mainData.get(arg2)
							.getAdditionalData().getComment());
					intent.putExtra("currentView", "show");
					startActivity(intent);
				}
			});
		}

	}

	@Override
	public void callbackNewMainData(boolean inserted) {
		// TODO Auto-generated method stub

	}

	@Override
	public void callbackDeleteMainData(boolean deleted) {
		for (int i = (selected.size() - 1); i >= 0; i--) {
			if (selected.valueAt(i)) {
				mainData selecteditem = adapter.getItem(selected.keyAt(i));
				// Remove selected items following the ids
				adapter.remove(selecteditem);
			}
		}
		Toast.makeText(
				getActivity(),
				String.format(getActivity().getString(R.string.deletedCount),
						selected.size()), Toast.LENGTH_SHORT).show();
	}

	@Override
	public void callbackUpdatedMainData(boolean updated) {
		// TODO Auto-generated method stub

	}

}
