package com.example.winfo;

public class AdditionalData {

	private int id;
	private String comment;

	public AdditionalData(int id, String comment) {
		super();
		this.id = id;
		this.comment = comment;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

}
