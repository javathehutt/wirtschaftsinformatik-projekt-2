package com.example.winfo;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import android.content.Context;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class CustomListAdapter extends ArrayAdapter<mainData> {
	private final Context context;
	private final ArrayList<mainData> mainDataList;
    private SparseBooleanArray mSelectedItemsIds;

	public CustomListAdapter(Context context, ArrayList<mainData> arrayList) {

		super(context, R.layout.list_item, arrayList);
		mSelectedItemsIds = new SparseBooleanArray();
		this.context = context;
		this.mainDataList = arrayList;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		// 1. Create inflater
		LayoutInflater inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		// 2. Get rowView from inflater
		View rowView = inflater.inflate(R.layout.list_item, parent, false);

		// 3. Get the two text view from the rowView
		TextView dateView = (TextView) rowView.findViewById(R.id.date);
		TextView workinghoursView = (TextView) rowView
				.findViewById(R.id.workingHours);
		TextView worktimeView = (TextView) rowView.findViewById(R.id.worktime);
		TextView activityHeaderView = (TextView) rowView
				.findViewById(R.id.activityName);
		TextView entityNameView = (TextView) rowView
				.findViewById(R.id.entityName);
		// 4. Set the text for textView

		String dateString = mainDataList.get(position).getStartTime();

		// Umwandeln des Timestamps
		SimpleDateFormat userDateFormat = new SimpleDateFormat(
				"yyyy-MM-dd HH:mm:SS");
		SimpleDateFormat dateFormatNeeded = new SimpleDateFormat(
				" dd. MMM yyyy HH:mm");
		userDateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
		dateFormatNeeded.setTimeZone(TimeZone.getTimeZone("GMT"));
		Date date = null;
		try {
			date = userDateFormat.parse(dateString);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String finaldate = dateFormatNeeded.format(date);

		// Umwandeln der Pause ins Date Format
		SimpleDateFormat userDateFormatPause = new SimpleDateFormat("HH:mm:SS");
		SimpleDateFormat dateFormatNeededPause = new SimpleDateFormat("HH.mm");
		userDateFormatPause.setTimeZone(TimeZone.getTimeZone("GMT"));
		dateFormatNeededPause.setTimeZone(TimeZone.getTimeZone("GMT"));
		
		Date datePause = null;
		try {
			datePause = userDateFormatPause.parse(mainDataList.get(position)
					.getPause());
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		String finalPause = dateFormatNeededPause.format(datePause);

		// Umwandeln der Zeit des Timestamps ins Date Format
		SimpleDateFormat userDateFormatTime = new SimpleDateFormat(
				"yyyy-MM-dd HH:mm:SS");
		SimpleDateFormat dateFormatNeededTime = new SimpleDateFormat("HH.mm");
		userDateFormatTime.setTimeZone(TimeZone.getTimeZone("GMT"));
		dateFormatNeededTime.setTimeZone(TimeZone.getTimeZone("GMT"));
		Date dateStartTime = null;
		Date dateEndTime = null;
		try {
			dateStartTime = userDateFormatTime.parse(mainDataList.get(position)
					.getStartTime());
			dateEndTime = userDateFormatTime.parse(mainDataList.get(position)
					.getEndTime());
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		String finalStartTime = dateFormatNeededTime.format(dateStartTime);
		String finalEndTime = dateFormatNeededTime.format(dateEndTime);

		// float result = Float.parseFloat(finalEndTime) -
		// Float.parseFloat(finalStartTime)-Float.parseFloat(finalPause);
		// String worktime = String.valueOf(result);

		long difference = dateEndTime.getTime() - dateStartTime.getTime()
				- datePause.getTime();
		int days = (int) (difference / (1000 * 60 * 60 * 24));
		int hours = (int) (difference / (1000 * 60 * 60) % 24);
		int minutes = (int) (difference / (1000 * 60) % 60);
		int seconds = (int) (difference / 1000 % 60);
		int millis = (int) (difference % 1000);

		String worktime;
		if (hours < 10) {
			if (minutes < 10) {
				worktime = "0" + String.valueOf(hours) + ":" + "0"
						+ String.valueOf(minutes);
			} else {
				worktime = "0" + String.valueOf(hours) + ":"
						+ String.valueOf(minutes);
			}
		} else {
			if (minutes < 10) {
				worktime = String.valueOf(hours) + ":" + "0"
						+ String.valueOf(minutes);
			} else {
				worktime = String.valueOf(hours) + ":"
						+ String.valueOf(minutes);
			}
		}

		dateView.setText(finaldate.substring(0, 13));
		workinghoursView
				.setText(mainDataList.get(position).getStartTime()
						.substring(10, 16)
						+ " -"
						+ mainDataList.get(position).getEndTime()
								.substring(10, 16));
		worktimeView.setText(worktime);
		entityNameView.setText(mainDataList.get(position).getActivity()
				.getEntity().getName());
		activityHeaderView.setText(mainDataList.get(position).getActivity()
				.getActivityHeader());
		// 5. return rowView
		return rowView;
	}
	
    @Override
    public void remove(mainData object) {
        mainDataList.remove(object);
        notifyDataSetChanged();
    }
 
    public List<mainData> getMainData() {
        return mainDataList;
    }
 
    public void toggleSelection(int position) {
        selectView(position, !mSelectedItemsIds.get(position));
    }
 
    public void removeSelection() {
        mSelectedItemsIds = new SparseBooleanArray();
        notifyDataSetChanged();
    }
 
    public void selectView(int position, boolean value) {
        if (value)
            mSelectedItemsIds.put(position, value);
        else
            mSelectedItemsIds.delete(position);
        notifyDataSetChanged();
    }
 
    public int getSelectedCount() {
        return mSelectedItemsIds.size();
    }
 
    public SparseBooleanArray getSelectedIds() {
        return mSelectedItemsIds;
    }
	
}
