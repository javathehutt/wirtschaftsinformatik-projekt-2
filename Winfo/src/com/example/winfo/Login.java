package com.example.winfo;

import java.util.ArrayList;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

@SuppressLint("ShowToast")
public class Login extends Activity {

	public static final String UserPREFERENCES = "UserPrefs";
	SharedPreferences sharedpreferences;
	Button loginButton;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login);
		
		sharedpreferences = getSharedPreferences(UserPREFERENCES,
				Context.MODE_PRIVATE);
		if (sharedpreferences.contains("apiKey")) {
			Intent intent = new Intent(Login.this, MainActivity.class);
			startActivity(intent);
		}
		
		ActionBar bar = getActionBar();
		bar.setBackgroundDrawable(new ColorDrawable(0xFFFFA500));
	}


	public void login(View view) {
		// Logindaten holen
		EditText editText_email = (EditText) findViewById(R.id.editText_email);
		String email = editText_email.getText().toString();
		EditText editText_password = (EditText) findViewById(R.id.editText_password);
		String password = editText_password.getText().toString();
		loginButton = (Button) findViewById(R.id.buttonLogin);
		loginButton.setEnabled(false);
		LoginVerification login = new LoginVerification();
		login.execute(email, password);

	}

	public class LoginVerification extends AsyncTask<String, Void, Boolean> {

		@Override
		protected Boolean doInBackground(String... params) {

			Boolean error = null;
			String apiKey = null;
			Boolean loginSuccessful = false;
			String email = params[0];
			String password = params[1];

			ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
			nameValuePairs.add(new BasicNameValuePair("email", email));
			nameValuePairs.add(new BasicNameValuePair("password", password));

			// Create the HTTP request
			HttpParams httpParameters = new BasicHttpParams();

			// Setup timeouts
			HttpConnectionParams.setConnectionTimeout(httpParameters, 15000);
			HttpConnectionParams.setSoTimeout(httpParameters, 15000);

			HttpClient httpclient = new DefaultHttpClient(httpParameters);
			HttpPost httppost = new HttpPost(
					"http://winfo-androidhft.rhcloud.com/v1/login");
			try {

				HttpResponse response;
				httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
				response = httpclient.execute(httppost);

				HttpEntity entity = response.getEntity();
				
				String result = null;
				result = EntityUtils.toString(entity);
				// Create a JSON object from the request response
				JSONObject jsonObject = new JSONObject(result);

				// Retrieve the data from the JSON object
				error = jsonObject.getBoolean("error");
				apiKey = jsonObject.getString("apiKey");

				Editor editor = sharedpreferences.edit();
				editor.putString("apiKey", apiKey);
				editor.commit();

			} catch (Exception e) {
				e.printStackTrace();
			}

			if (error == false) {
				loginSuccessful = true;
			}
			return loginSuccessful;

		}

		protected void onPostExecute(Boolean loginSuccessful) {
			if (loginSuccessful) {
				Intent intent = new Intent(Login.this, MainActivity.class);
				intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivity(intent);
			} else {
				loginButton.setEnabled(true);
				Context context = getApplicationContext();
				Toast.makeText(context, context.getString(R.string.loginError),
						Toast.LENGTH_SHORT).show();
			}

		}

	}

}
