package com.example.winfo;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

import android.os.AsyncTask;

public class UpdateEntry extends AsyncTask<String, Void, Boolean> {

	private boolean updated = false;

	@Override
	protected Boolean doInBackground(String... params) {

		String startTime = params[0];
		String endTime = params[1];
		String pause = params[2];
		String activity = params[3];
		String additionalData = params[4];
		String comment = params[5];
		String mainDataID = params[6];
		String apiKey = params[7];

		if (additionalData.equals("0")) {
			if (!comment.isEmpty()) {
				try {
					HttpResponse response;
					HttpClient client = new DefaultHttpClient();
					HttpPut put = new HttpPut(
							"http://winfo-androidhft.rhcloud.com/v1/dataAddComment/"
									+ mainDataID);

					List<NameValuePair> pairs = new ArrayList<NameValuePair>();
					pairs.add(new BasicNameValuePair("startTime", startTime));
					pairs.add(new BasicNameValuePair("endTime", endTime));
					pairs.add(new BasicNameValuePair("pause", pause));
					pairs.add(new BasicNameValuePair("activity", activity));
					pairs.add(new BasicNameValuePair("comment", comment));
					put.setEntity(new UrlEncodedFormEntity(pairs));
					put.setHeader("Authorization", apiKey);
					
					response = client.execute(put);

					HttpEntity entity = response.getEntity();

					String result = null;
					result = EntityUtils.toString(entity);
					// Create a JSON object from the request response
					// Retrieve the data from the JSON object
					JSONObject jObj = new JSONObject(result);
					
					boolean error = jObj.getBoolean("error");
					if (error == false){
						updated = true;
					}

				} catch (Exception e) {
					e.printStackTrace();
				}

			} else {
				try {
					HttpResponse response;
					HttpClient client = new DefaultHttpClient();
					HttpPut put = new HttpPut(
							"http://winfo-androidhft.rhcloud.com/v1/data/"
									+ mainDataID);

					List<NameValuePair> pairs = new ArrayList<NameValuePair>();
					pairs.add(new BasicNameValuePair("startTime", startTime));
					pairs.add(new BasicNameValuePair("endTime", endTime));
					pairs.add(new BasicNameValuePair("pause", pause));
					pairs.add(new BasicNameValuePair("activity", activity));
					put.setEntity(new UrlEncodedFormEntity(pairs));
					put.setHeader("Authorization", apiKey);

					response = client.execute(put);

					HttpEntity entity = response.getEntity();

					String result = null;
					result = EntityUtils.toString(entity);
					// Create a JSON object from the request response
					// Retrieve the data from the JSON object
					JSONObject jObj = new JSONObject(result);
					
					boolean error = jObj.getBoolean("error");
					if (error == false){
						updated = true;
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		} else {
			try {
				HttpResponse response;
				HttpClient client = new DefaultHttpClient();
				HttpPut put = new HttpPut(
						"http://winfo-androidhft.rhcloud.com/v1/data/"
								+ mainDataID);

				List<NameValuePair> pairs = new ArrayList<NameValuePair>();
				pairs.add(new BasicNameValuePair("startTime", startTime));
				pairs.add(new BasicNameValuePair("endTime", endTime));
				pairs.add(new BasicNameValuePair("pause", pause));
				pairs.add(new BasicNameValuePair("activity", activity));
				pairs.add(new BasicNameValuePair("additionalData",
						additionalData));
				pairs.add(new BasicNameValuePair("comment", comment));
				put.setEntity(new UrlEncodedFormEntity(pairs));
				put.setHeader("Authorization", apiKey);

				response = client.execute(put);

				HttpEntity entity = response.getEntity();

				String result = null;
				result = EntityUtils.toString(entity);
				// Create a JSON object from the request response
				// Retrieve the data from the JSON object
				JSONObject jObj = new JSONObject(result);
				
				boolean error = jObj.getBoolean("error");
				if (error == false){
					updated = true;
				}
				
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		return updated;
	}

	@Override
	protected void onPostExecute(Boolean updated) {
		mListener.callbackUpdatedMainData(updated);
	}

	CallBackListener mListener;
	
	public void setListener(CallBackListener listener){
		mListener = listener;
	}
	
}
