package com.example.winfo;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import android.R.color;
import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.Fragment;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

public class ListViewItemDetails extends Activity implements CallBackListener {

	static View rootView;
	private ActionBar actionBar;
	static Button dateText;
	static Button startTimeText;
	static Button endTimeText;
	static Button pauseText;
	static EditText workTimeText;
	static EditText commentText;
	static TextView workTimeLabelText;
	static Spinner entity_spinner;
	static Spinner activity_spinner;
	static String activityId;
	private static ArrayList<MyActivity> activitiesOriginal = new ArrayList<MyActivity>();
	private static ArrayList<MyActivity> activitiesSelected = new ArrayList<MyActivity>();
	private static ArrayList<MyEntity> entitiesOriginal = new ArrayList<MyEntity>();
	ListView list;
	public static final String UserPREFERENCES = "UserPrefs";
	SharedPreferences sharedpreferences;
	public static String apiKey;
	static MenuItem action_edit;
	MenuItem action_save;
	static MyActivity selected_activity;
	static MyEntity selected_entity;
	public static String currentView = "show";
	static MyEntity currentEntity;
	static MyActivity currentActivity;
	static int additionalData;
	static String idmainData;
	static int fromWhere;
	static String newComment = "true";
	static ArrayAdapter<MyActivity> activityDataAdapter;
	static String activityHeaderText;
	static String entityText;
	static boolean editMode = false;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_list_view_item_details);

		sharedpreferences = getSharedPreferences(UserPREFERENCES,
				Context.MODE_PRIVATE);
		apiKey = sharedpreferences.getString("apiKey", "");

		if (savedInstanceState == null) {
			getFragmentManager().beginTransaction()
					.add(R.id.container, new PlaceholderFragment()).commit();
		}
		actionBar = getActionBar();
		actionBar.setBackgroundDrawable(new ColorDrawable(0xFFFFA500));
		actionBar.setDisplayHomeAsUpEnabled(true);
		activitiesOriginal.clear();
		entitiesOriginal.clear();

		// activitiesOriginal.add(new MyActivity(0, "Please Select activity",
		// "",
		// 0));
		// entitiesOriginal.add(new MyEntity(0, "Please select entity"));
		// activitiesSelected.add(new MyActivity(0, "Please select",
		// "", 0));
		FetchData fetch = new FetchData();
		fetch.execute(apiKey);

	}

	public boolean onOptionsItemSelected(MenuItem item) {

		switch (item.getItemId()) {
		case R.id.action_edit:

			action_save.setVisible(true);
			action_edit.setVisible(false);

			// Set all EditText elements editable
			enableEdit();

		case R.id.home:
			return true;

		case R.id.action_save:

			if (checkForValidEntries()) {
				if (commentText.getText().length() == 0) {
					newComment = "false";
				}

				// Umwandeln des Timestamps in ein Datum
				SimpleDateFormat userDateFormat = new SimpleDateFormat(
						"dd.MM.yyyy");
				SimpleDateFormat dateFormatNeeded = new SimpleDateFormat(
						"yyyy-MM-dd");
				Date date = null;

				try {
					date = userDateFormat.parse(dateText.getText().toString());
				} catch (ParseException e) {
					e.printStackTrace();
				}
				String finaldate = dateFormatNeeded.format(date);

				String finalStartTimestamp = finaldate + " "
						+ startTimeText.getText().toString();
				String finalEndTimestamp = finaldate + " "
						+ endTimeText.getText().toString();

				String pauseString = pauseText.getText().toString() + ":00";

				if (currentView.equals("show")) {
					UpdateEntry update = new UpdateEntry();
					update.setListener(this);
					update.execute(finalStartTimestamp, finalEndTimestamp,
							pauseString, activityId, String
									.valueOf(additionalData), commentText
									.getText().toString(), idmainData, apiKey);

					action_save.setVisible(false);
					action_edit.setVisible(true);
				} else {

					CreateMainData createEntry = new CreateMainData();
					createEntry.setListener(this);
					createEntry.execute(apiKey, finalStartTimestamp,
							finalEndTimestamp, pauseString,
							String.valueOf(selected_activity.getId()),
							commentText.getText().toString(), newComment);
				}

				// Set all EditText elements not editable
				disableEdit();
			} else {
				Toast.makeText(getApplicationContext(),
						"Bitte Pflichtfelder ausf�llen!", Toast.LENGTH_LONG)
						.show();
			}
		}

		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onBackPressed() {
		if (editMode) {
			new AlertDialog.Builder(this)
					.setTitle("Eingabe verwerfen")
					.setMessage(R.string.cancelEdit)
					.setPositiveButton(android.R.string.yes,
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,
										int which) {
									ListViewItemDetails.super.onBackPressed();
								}
							})
					.setNegativeButton(android.R.string.no,
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,
										int which) {

								}
							}).show();
		} else {
			super.onBackPressed();
		}

	}

	private boolean checkForValidEntries() {
		boolean isValid = true;
		if (dateText.getText().length() == 0) {
			isValid = false;
		}
		if (startTimeText.getText().length() == 0) {
			isValid = false;
		}
		if (endTimeText.getText().length() == 0) {
			isValid = false;
		}
		if (pauseText.getText().length() == 0) {
			isValid = false;
		}

		if (!currentView.equals("show")) {
			if (entity_spinner.getSelectedItemPosition() == 0) {
				isValid = false;
			}
			if (activity_spinner.getSelectedItemPosition() == 0) {
				isValid = false;
			}
		}

		return isValid;
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.list_view_item_details, menu);

		action_edit = menu.findItem(R.id.action_edit);
		action_save = menu.findItem(R.id.action_save);
		action_edit.setVisible(true);
		return true;

	}

	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {

		if (currentView.equals("add")) {
			action_edit = menu.findItem(R.id.action_edit);
			action_save = menu.findItem(R.id.action_save);
			action_edit.setVisible(false);
			action_save.setVisible(true);

		}
		return super.onPrepareOptionsMenu(menu);
	}

	public void showDatePickerDialog(View v) {
		DialogFragment newFragment = new DatePickerFragment();
		newFragment.show(getFragmentManager(), "datePicker");
	}

	public void showTimePickerDialog(View v) {
		DialogFragment newFragment = new TimePickerFragment();
		fromWhere = v.getId();
		newFragment.show(getFragmentManager(), "timePicker");

	}

	/**
	 * A placeholder fragment containing a simple view.
	 */
	public static class PlaceholderFragment extends Fragment implements
			OnItemSelectedListener {

		public PlaceholderFragment() {
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			rootView = inflater.inflate(
					R.layout.fragment_list_view_item_details, container, false);

			dateText = (Button) rootView.findViewById(R.id.date);
			startTimeText = (Button) rootView.findViewById(R.id.startTime);
			endTimeText = (Button) rootView.findViewById(R.id.endTime);
			pauseText = (Button) rootView.findViewById(R.id.pause);
			workTimeText = (EditText) rootView.findViewById(R.id.workTime);
			commentText = (EditText) rootView.findViewById(R.id.comment);
			entity_spinner = (Spinner) rootView.findViewById(R.id.entity);
			activity_spinner = (Spinner) rootView
					.findViewById(R.id.activityHeader);

			TextView workTimeLabelText = (TextView) rootView
					.findViewById(R.id.workTimeLabel);

			Intent intent = getActivity().getIntent();
			Bundle b = intent.getExtras();

			String apiKey = null;

			if (b.getString("currentView").equals("show")) {

				idmainData = String.valueOf(b.getInt("idmainData"));
				String dateString = b.getString("startTime");
				String endTimeString = b.getString("endTime");
				String pauseString = b.getString("pause");
				currentView = b.getString("currentView");
				activityId = b.getString("activityId");
				additionalData = b.getInt("additionalData");
				activityHeaderText = b.getString("activityHeader");
				entityText = b.getString("entity");

				// Umwandeln des Timestamps in ein Datum
				SimpleDateFormat userDateFormat = new SimpleDateFormat(
						"yyyy-MM-dd HH:mm:SS");
				SimpleDateFormat dateFormatNeeded = new SimpleDateFormat(
						" dd.MM.yyyy");
				userDateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
				dateFormatNeeded.setTimeZone(TimeZone.getTimeZone("GMT"));
				Date date = null;

				try {
					date = userDateFormat.parse(dateString);
				} catch (ParseException e) {
					e.printStackTrace();
				}
				String finaldate = dateFormatNeeded.format(date);

				// Umwandeln der Zeit des Timestamps ins Date Format
				SimpleDateFormat userDateFormatTime = new SimpleDateFormat(
						"yyyy-MM-dd HH:mm:SS");
				SimpleDateFormat dateFormatNeededTime = new SimpleDateFormat(
						"HH:mm");
				userDateFormatTime.setTimeZone(TimeZone.getTimeZone("GMT"));
				dateFormatNeededTime.setTimeZone(TimeZone.getTimeZone("GMT"));
				Date dateStartTime = null;
				Date dateEndTime = null;
				try {
					dateStartTime = userDateFormatTime.parse(dateString);
					dateEndTime = userDateFormatTime.parse(endTimeString);
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				String finalStartTime = dateFormatNeededTime
						.format(dateStartTime);
				String finalEndTime = dateFormatNeededTime.format(dateEndTime);

				// Umwandeln der Pause ins Date Format
				SimpleDateFormat userDateFormatPause = new SimpleDateFormat(
						"HH:mm:SS");
				SimpleDateFormat dateFormatNeededPause = new SimpleDateFormat(
						"HH:mm");
				userDateFormatPause.setTimeZone(TimeZone.getTimeZone("GMT"));
				dateFormatNeededPause.setTimeZone(TimeZone.getTimeZone("GMT"));
				Date datePause = null;
				try {
					datePause = userDateFormatPause.parse(pauseString);
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				String finalPause = dateFormatNeededPause.format(datePause);

				long difference = dateEndTime.getTime()
						- dateStartTime.getTime() - datePause.getTime();
				int days = (int) (difference / (1000 * 60 * 60 * 24));
				int hours = (int) (difference / (1000 * 60 * 60) % 24);
				int minutes = (int) (difference / (1000 * 60) % 60);
				int seconds = (int) (difference / 1000 % 60);
				int millis = (int) (difference % 1000);

				String worktime;
				if (hours < 10) {
					if (minutes < 10) {
						worktime = "0" + String.valueOf(hours) + ":" + "0"
								+ String.valueOf(minutes);
					} else {
						worktime = "0" + String.valueOf(hours) + ":"
								+ String.valueOf(minutes);
					}
				} else {
					if (minutes < 10) {
						worktime = String.valueOf(hours) + ":" + "0"
								+ String.valueOf(minutes);
					} else {
						worktime = String.valueOf(hours) + ":"
								+ String.valueOf(minutes);
					}
				}

				// Set all EditText elements not editable
				disableEdit();

				// Set the text of EditText elements
				dateText.setText(finaldate);
				startTimeText.setText(finalStartTime);
				endTimeText.setText(finalEndTime);
				workTimeText.setText(worktime);
				pauseText.setText(finalPause);
				if (b.getString("comment").equals("null")) {
					commentText.setText("");
				} else {
					commentText.setText(b.getString("comment"));
				}

				activitiesOriginal.add(new MyActivity(0, activityHeaderText,
						"", 0));
				entitiesOriginal.add(new MyEntity(0, entityText));
				activitiesSelected.clear();
				activitiesSelected.add(new MyActivity(0, activityHeaderText,
						"", 0));

				activity_spinner.setBackgroundColor(Color.TRANSPARENT);
				entity_spinner.setBackgroundColor(Color.TRANSPARENT);

				Spinner_Activity();
				Spinner_Entities();
				activity_spinner.setClickable(false);
				entity_spinner.setClickable(false);
				return rootView;

			} else {
				enableEdit();
				currentView = b.getString("currentView");
				getActivity().invalidateOptionsMenu();
				workTimeLabelText.setVisibility(android.view.View.GONE);
				workTimeText.setVisibility(android.view.View.GONE);

				activitiesOriginal.add(new MyActivity(0, "Bitte ausw�hlen", "",
						0));
				entitiesOriginal.add(new MyEntity(0, "Bitte ausw�hlen"));
				activitiesSelected.clear();
				activitiesSelected.add(new MyActivity(0, "Bitte ausw�hlen", "",
						0));

				Spinner_Activity();
				Spinner_Entities();
				activity_spinner.setClickable(false);
				entity_spinner.setClickable(false);
				return rootView;

			}

		}

		@Override
		public void onItemSelected(AdapterView<?> parent, View view,
				int position, long id) {
			if (position != 0) {

				switch (parent.getId()) {
				case R.id.entity:
					selected_entity = (MyEntity) parent
							.getItemAtPosition(position);
					activitiesSelected.clear();
					activitiesSelected.add(new MyActivity(0, "Bitte ausw�hlen",
							"", 0));
					for (int j = 0; j < activitiesOriginal.size(); j++) {
						if (activitiesOriginal.get(j).getEntityID() == selected_entity
								.getId()) {
							activitiesSelected.add(activitiesOriginal.get(j));
						}
					}
					activity_spinner.setSelection(0);
					activityDataAdapter.setNotifyOnChange(true);
					break;
				case R.id.activityHeader:
					selected_activity = (MyActivity) parent
							.getItemAtPosition(position);
					break;
				}

				// On selecting a spinner item
				String item = parent.getItemAtPosition(position).toString();

				// Showing selected spinner item
				Toast.makeText(parent.getContext(), "Ausgew�hlt: " + item,
						Toast.LENGTH_LONG).show();
			}

		}

		@Override
		public void onNothingSelected(AdapterView<?> parent) {
			// TODO Auto-generated method stub

		}

		public void Spinner_Entities() {
			entity_spinner = (Spinner) rootView.findViewById(R.id.entity);

			// Spinner click listener

			entity_spinner.setOnItemSelectedListener(this);
			Log.d("spinnerentity", entity_spinner.toString());

			ArrayAdapter<MyEntity> dataAdapter = new ArrayAdapter<MyEntity>(
					rootView.getContext(),
					android.R.layout.simple_spinner_item, entitiesOriginal);

			// Drop down layout style - list view with radio button
			dataAdapter
					.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

			// attaching data adapter to spinner
			entity_spinner.setAdapter(dataAdapter);

		}

		public void Spinner_Activity() {
			activity_spinner = (Spinner) rootView
					.findViewById(R.id.activityHeader);
			activity_spinner.setOnItemSelectedListener(this);

			// Spinner Drop down elements -

			// Creating adapter for spinner
			activityDataAdapter = new ArrayAdapter<MyActivity>(
					rootView.getContext(),
					android.R.layout.simple_spinner_item, activitiesSelected);

			// Drop down layout style - list view with radio button
			activityDataAdapter
					.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

			// attaching data adapter to spinner
			activity_spinner.setAdapter(activityDataAdapter);

			// Creating adapter for spinner

		}
	}

	public class FetchData extends AsyncTask<String, Void, Void> {

		@Override
		protected Void doInBackground(String... params) {

			String apiKey = params[0];

			HttpClient httpclient = new DefaultHttpClient();
			HttpGet httpget = new HttpGet(
					"http://winfo-androidhft.rhcloud.com/v1/entityActivity");

			try {
				Log.d("myApp", "before Response");
				HttpResponse response;
				httpget.setHeader("Authorization", apiKey);
				response = httpclient.execute(httpget);
				Log.d("myApp", "after execute");

				HttpEntity entity = response.getEntity();

				String result = null;
				result = EntityUtils.toString(entity);
				// Create a JSON object from the request response
				// Retrieve the data from the JSON object
				JSONObject jObj = new JSONObject(result);
				JSONArray jArr = jObj.getJSONArray("data");
				for (int i = 0; i < jArr.length(); i++) {
					JSONObject obj = jArr.getJSONObject(i);
					activitiesOriginal.add(new MyActivity(obj
							.getInt("idActivity"), obj
							.getString("activityHeader"), obj
							.getString("activityDescription"), obj
							.getInt("entity")));

					int entityID = obj.getInt("entity");
					String entityName = obj.getString("entityName");
					MyEntity myentity = new MyEntity(entityID, entityName);

					boolean entityExists = false;
					for (int j = 0; j < entitiesOriginal.size(); j++) {
						if (entitiesOriginal.get(j).getId() == myentity.getId()) {
							entityExists = true;
						}
					}
					if (entityExists == false) {
						entitiesOriginal.add(myentity);
					}

				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			activity_spinner.setClickable(true);
			entity_spinner.setClickable(true);
			return null;

		}

	}

	public static void enableEdit() {
		editMode = true;
		dateText.setClickable(true);
		dateText.setBackgroundResource(R.drawable.apptheme_textfield_focused_holo_light);

		startTimeText.setClickable(true);
		startTimeText
				.setBackgroundResource(R.drawable.apptheme_textfield_focused_holo_light);

		endTimeText.setClickable(true);
		endTimeText
				.setBackgroundResource(R.drawable.apptheme_textfield_focused_holo_light);

		pauseText.setClickable(true);
		pauseText.setCursorVisible(true);
		pauseText.setFocusable(true);
		pauseText.setFocusableInTouchMode(true);
		pauseText
				.setBackgroundResource(R.drawable.apptheme_textfield_focused_holo_light);

		commentText.setClickable(true);
		commentText.setCursorVisible(true);
		commentText.setFocusable(true);
		commentText.setFocusableInTouchMode(true);
		commentText
				.setBackgroundResource(R.drawable.apptheme_textfield_focused_holo_light);

	}

	public static void disableEdit() {
		editMode = false;
		dateText.setClickable(false);
		dateText.setCursorVisible(false);
		dateText.setFocusable(false);
		dateText.setFocusableInTouchMode(false);
		dateText.setBackgroundResource(android.R.color.transparent);

		startTimeText.setClickable(false);
		startTimeText.setCursorVisible(false);
		startTimeText.setFocusable(false);
		startTimeText.setFocusableInTouchMode(false);
		startTimeText.setBackgroundResource(android.R.color.transparent);

		endTimeText.setClickable(false);
		endTimeText.setCursorVisible(false);
		endTimeText.setFocusable(false);
		endTimeText.setFocusableInTouchMode(false);
		endTimeText.setBackgroundResource(android.R.color.transparent);

		pauseText.setClickable(false);
		pauseText.setCursorVisible(false);
		pauseText.setFocusable(false);
		pauseText.setFocusableInTouchMode(false);
		pauseText.setBackgroundResource(android.R.color.transparent);

		workTimeText.setClickable(false);
		workTimeText.setCursorVisible(false);
		workTimeText.setFocusable(false);
		workTimeText.setFocusableInTouchMode(false);
		workTimeText.setBackgroundResource(android.R.color.transparent);

		commentText.setClickable(false);
		commentText.setCursorVisible(false);
		commentText.setFocusable(false);
		commentText.setFocusableInTouchMode(false);
		commentText.setBackgroundResource(android.R.color.transparent);

		entity_spinner.setEnabled(false);
		activity_spinner.setEnabled(false);
	}

	public static class DatePickerFragment extends DialogFragment implements
			DatePickerDialog.OnDateSetListener {

		@Override
		public Dialog onCreateDialog(Bundle savedInstanceState) {
			// Use the current date as the default date in the picker
			final Calendar c = Calendar.getInstance();
			int year = c.get(Calendar.YEAR);
			int month = c.get(Calendar.MONTH);
			int day = c.get(Calendar.DAY_OF_MONTH);

			// Create a new instance of DatePickerDialog and return it
			return new DatePickerDialog(getActivity(), this, year, month, day);
		}

		public void onDateSet(DatePicker view, int year, int month, int day) {
			month = month + 1;
			String dateString = day + "." + month + "." + year;
			dateText.setText(dateString);
		}
	}

	public static class TimePickerFragment extends DialogFragment implements
			TimePickerDialog.OnTimeSetListener {

		@Override
		public Dialog onCreateDialog(Bundle savedInstanceState) {
			// Use the current time as the default values for the picker
			int hour;
			int minute;
			switch (fromWhere) {
			case R.id.pause:
				hour = 0;
				minute = 0;
				break;
			default:
				final Calendar c = Calendar.getInstance();
				hour = c.get(Calendar.HOUR_OF_DAY);
				minute = c.get(Calendar.MINUTE);
			}

			// Create a new instance of TimePickerDialog and return it
			return new TimePickerDialog(getActivity(), this, hour, minute, true);
		}

		public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
			String time;
			if (hourOfDay < 10) {
				time = "0" + hourOfDay + ":" + minute;
				if (minute < 10) {
					time = "0" + hourOfDay + ":" + "0" + minute;
				}
			} else {
				time = hourOfDay + ":" + minute;
				if (minute < 10) {
					time = hourOfDay + ":" + "0" + minute;
				}
			}

			switch (fromWhere) {
			case R.id.pause:
				pauseText.setText(time);
				break;
			case R.id.startTime:
				startTimeText.setText(time);
				break;
			case R.id.endTime:
				endTimeText.setText(time);
				break;

			}

		}
	}

	@Override
	public void callbackNewMainData(boolean inserted) {
		if (inserted == true) {
			Toast.makeText(getApplicationContext(),
					getApplicationContext().getString(R.string.inserted),
					Toast.LENGTH_SHORT).show();
			Intent intent = new Intent(getApplicationContext(),
					MainActivity.class);
			intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(intent);
		} else {
			Toast.makeText(getApplicationContext(),
					getApplicationContext().getString(R.string.insertError),
					Toast.LENGTH_SHORT).show();
		}

	}

	@Override
	public void callbackDeleteMainData(boolean deleted) {
		// TODO Auto-generated method stub

	}

	@Override
	public void callbackUpdatedMainData(boolean updated) {
		if (updated == true) {
			Toast.makeText(getApplicationContext(),
					getApplicationContext().getString(R.string.updated),
					Toast.LENGTH_SHORT).show();
			Intent intent = new Intent(getApplicationContext(),
					MainActivity.class);
			intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(intent);
		} else {
			Toast.makeText(getApplicationContext(),
					getApplicationContext().getString(R.string.updateError),
					Toast.LENGTH_SHORT).show();
		}
	}

}
