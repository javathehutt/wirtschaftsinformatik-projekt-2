package com.example.winfo;

import java.util.Date;

public class mainData {
	private int id;
	private String startTime;
	private String endTime;
	private Date startTimeTime;
	private Date endTimeTime;
	private String pause;
	private Date pauseTime;
	private MyActivity activity;
	private AdditionalData additionalData;


	public mainData(int id, String startTime, String endTime, String pause,
			MyActivity activity) {
		this.id = id;
		this.startTime = startTime;
		this.endTime = endTime;
		this.pause = pause;
		this.activity = activity;
	}
	public mainData(int id, String startTime, String endTime, String pause,
			MyActivity activity, AdditionalData additionalData) {
		super();
		this.id = id;
		this.startTime = startTime;
		this.endTime = endTime;
		this.pause = pause;
		this.activity = activity;
		this.additionalData = additionalData;
	}
	
	public mainData(int id, Date startTimeTime, Date endTimeTime,
			Date pauseTime, MyActivity activity) {
		super();
		this.id = id;
		this.startTimeTime = startTimeTime;
		this.endTimeTime = endTimeTime;
		this.pauseTime = pauseTime;
		this.activity = activity;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	public String getPause() {
		return pause;
	}

	public void setPause(String date) {
		this.pause = date;
	}

	public MyActivity getActivity() {
		return activity;
	}

	public void setActivity(MyActivity activity) {
		this.activity = activity;
	}

	public Date getStartTimeTime() {
		return startTimeTime;
	}

	public void setStartTimeTime(Date startTimeTime) {
		this.startTimeTime = startTimeTime;
	}

	public Date getEndTimeTime() {
		return endTimeTime;
	}

	public void setEndTimeTime(Date endTimeTime) {
		this.endTimeTime = endTimeTime;
	}

	public Date getPauseTime() {
		return pauseTime;
	}

	public void setPauseTime(Date pauseTime) {
		this.pauseTime = pauseTime;
	}

	public AdditionalData getAdditionalData() {
		return additionalData;
	}

	public void setAdditionalData(AdditionalData additionalData) {
		this.additionalData = additionalData;
	}
	
}
