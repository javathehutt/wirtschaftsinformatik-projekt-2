package com.example.winfo;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.v4.app.Fragment;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Chronometer;
import android.widget.Chronometer.OnChronometerTickListener;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class StopFragment extends Fragment implements OnClickListener,
		CallBackListener, OnItemSelectedListener {
	// Uses chronometer widget
	private Chronometer chronometer;
	private Button startButton;
	private Button stopButton;
	private Button resetButton;
	private Button sendButton;
	private boolean stopbuttonclicked = false;
	private long timeWhenStopped = 0;
	static View rootView;
	private TextView textViewTime;
	boolean timeCounter = true;
	SharedPreferences sharedpreferences;
	public static String apiKey;
	public static final String UserPREFERENCES = "UserPrefs";
	private EditText txtField;
	ListView list;
	private Date startTimestamp;
	private Date endTimestamp;
	private Date pauseStart;
	private Date pauseEnd;
	private long pauseTemp = 0;
	private String startTimestampString;
	private String endTimestampString;
	private String pauseString;
	private Date date;
	private View line;
	private String newComment = "true";
	private Calendar calendar;
	MyActivity selected_activity;
	MyEntity selected_entity;
	Spinner entity_spinner;
	Spinner activity_spinner;
	ArrayAdapter<MyEntity> entityDataAdapter;
	ArrayAdapter<MyActivity> activityDataAdapter;

	private ArrayList<MyEntity> entitiesOriginal = new ArrayList<MyEntity>();
	private ArrayList<MyActivity> activitiesOriginal = new ArrayList<MyActivity>();
	private ArrayList<MyActivity> activitiesSelected = new ArrayList<MyActivity>();

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		rootView = inflater.inflate(R.layout.fragment_stop, container, false);
		chronometer = (Chronometer) rootView.findViewById(R.id.chronometer);
		chronometer.setText("00:00:00");
		chronometer.setOnChronometerTickListener(new OnChronometerTickListener(){
		    @Override
	        public void onChronometerTick(Chronometer cArg) {
	        long time = SystemClock.elapsedRealtime() - cArg.getBase();
	        int h   = (int)(time/3600000);
	        int m = (int)(time - h*3600000)/60000;
	        int s= (int)(time - h*3600000- m*60000)/1000 ;
	        String hh = h < 10 ? "0"+h: h+"";
	        String mm = m < 10 ? "0"+m: m+"";
	        String ss = s < 10 ? "0"+s: s+"";
	        cArg.setText(hh+":"+mm+":"+ss);
	    }
	});
		
		startButton = (Button) rootView.findViewById(R.id.start_button);
		stopButton = (Button) rootView.findViewById(R.id.stop_button);
		resetButton = (Button) rootView.findViewById(R.id.reset_button);
		sendButton = (Button) rootView.findViewById(R.id.send_button);

		txtField = (EditText) rootView.findViewById(R.id.txtField);
		txtField.setSelection(0);
		line = (View) rootView.findViewById(R.id.line);

		startButton.setOnClickListener(this);
		stopButton.setOnClickListener(this);
		resetButton.setOnClickListener(this);
		sendButton.setOnClickListener(this);

		line.setOnClickListener(this);

		stopButton.setVisibility(View.INVISIBLE);
		resetButton.setClickable(false);
		resetButton.setEnabled(false);
		sendButton.setEnabled(false);

		line.setVisibility(View.INVISIBLE);
		textViewTime = (TextView) rootView.findViewById(R.id.tv_time);

		sharedpreferences = getActivity().getSharedPreferences(UserPREFERENCES,
				Context.MODE_PRIVATE);
		apiKey = sharedpreferences.getString("apiKey", "");

		textViewTime = (TextView) rootView.findViewById(R.id.tv_time);
		entitiesOriginal.clear();
		activitiesOriginal.clear();
		entitiesOriginal.add(new MyEntity(0, "Bitte ausw�hlen"));
		activitiesSelected.add(new MyActivity(0, "Bitte ausw�hlen", "", 0));
		calendar = Calendar.getInstance();
		pauseStart = new Date();
		pauseEnd = new Date();
		Spinner_Activity();
		Spinner_Entities();
		entity_spinner.setEnabled(false);
		activity_spinner.setEnabled(false);

		FetchData fetch = new FetchData();
		fetch.execute(apiKey);
		return rootView;

	}

	// Methode die die Zeit berechnet
	@SuppressLint("SimpleDateFormat")
	public String ZeitundDatum() {
		calendar = Calendar.getInstance();
		date = calendar.getTime();
		startTimestamp = date;
		SimpleDateFormat simpleTime = new SimpleDateFormat("dd.MM.yyyy" + " "
				+ "HH:mm:ss");
		final String time_string = simpleTime.format(date);

		return time_string;

	}

	@SuppressLint("SimpleDateFormat")
	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.start_button:
			calendar = Calendar.getInstance();
			chronometer
					.setBase(SystemClock.elapsedRealtime() + timeWhenStopped);
			chronometer.start();
			startButton.setVisibility(View.INVISIBLE);
			stopButton.setVisibility(View.VISIBLE);
			resetButton.setClickable(false);
			resetButton.setEnabled(false);
			sendButton.setEnabled(false);

			pauseEnd = calendar.getTime();
			pauseTemp = pauseTemp + (pauseEnd.getTime() - pauseStart.getTime());

			if (timeCounter) {
				textViewTime.setText("erfasst am:     " + ZeitundDatum());
				line.setVisibility(View.VISIBLE);
				timeCounter = false;
			}
			break;
		case R.id.stop_button:
			stopbuttonclicked = true;
			calendar = Calendar.getInstance();
			timeWhenStopped = (int) (chronometer.getBase() - SystemClock
					.elapsedRealtime());
			endTimestamp = new Date();
			endTimestamp.setTime(calendar.getTime().getTime());
			pauseStart = calendar.getTime();
			chronometer.stop();
			startButton.setVisibility(View.VISIBLE);
			stopButton.setVisibility(View.INVISIBLE);
			resetButton.setClickable(true);
			resetButton.setEnabled(true);
			sendButton.setEnabled(true);
			break;
		case R.id.reset_button:
			
			chronometer.setBase(SystemClock.elapsedRealtime());
			chronometer.stop();
			timeWhenStopped = 0;
			startButton.setVisibility(View.VISIBLE);
			stopButton.setVisibility(View.INVISIBLE);
			textViewTime.setText(null);
			line.setVisibility(View.INVISIBLE);
			timeCounter = true;
			chronometer.setText("00:00:00");
			break;
		case R.id.send_button:
			
			if (checkForValidEntries()) {
			String comment = txtField.getText().toString();

			if (comment.isEmpty()) {
				newComment = "false";
			}
			setTimestampsAndPause(startTimestamp, endTimestamp, pauseTemp);
			String activityID = String.valueOf(selected_activity.getId());
			CreateMainData createMainData = new CreateMainData();
			createMainData.setListener(this);
			createMainData.execute(apiKey, startTimestampString,
					endTimestampString, pauseString, activityID, comment,
					newComment);
			
			chronometer.setBase(SystemClock.elapsedRealtime());
			chronometer.stop();
			timeWhenStopped = 0;
			textViewTime.setText(null);
			line.setVisibility(View.INVISIBLE);
			chronometer.setText("00:00:00");
			timeCounter = true;
			} else {
				Toast.makeText(getActivity(),
						"Bitte Pflichtfelder ausf�llen!", Toast.LENGTH_LONG)
						.show();
			}

			break;

		}
	}

	@Override
	public void onItemSelected(AdapterView<?> parent, View view, int position,
			long id) {
		if (position != 0) {

			switch (parent.getId()) {
			case R.id.entity_spinner:
				selected_entity = (MyEntity) parent.getItemAtPosition(position);
				activitiesSelected.clear();
				activitiesSelected
						.add(new MyActivity(0, "Bitte ausw�hlen", "", 0));
				for (int j = 0; j < activitiesOriginal.size(); j++) {
					if (activitiesOriginal.get(j).getEntityID() == selected_entity
							.getId()) {
						activitiesSelected.add(activitiesOriginal.get(j));
					}
				}
				activity_spinner.setSelection(0);
				activityDataAdapter.setNotifyOnChange(true);
				break;
			case R.id.activity_spinner:
				selected_activity = (MyActivity) parent
						.getItemAtPosition(position);
				break;
			}

			// On selecting a spinner item
			String item = parent.getItemAtPosition(position).toString();

			// Showing selected spinner item
			Toast.makeText(parent.getContext(), "Selected: " + item,
					Toast.LENGTH_LONG).show();
		}

	}

	@Override
	public void onNothingSelected(AdapterView<?> parent) {
		// TODO Auto-generated method stub

	}

	public void Spinner_Entities() {
		entity_spinner = (Spinner) rootView.findViewById(R.id.entity_spinner);

		// Spinner click listener

		entity_spinner.setOnItemSelectedListener(this);

		entityDataAdapter = new ArrayAdapter<MyEntity>(rootView.getContext(),
				android.R.layout.simple_spinner_item, entitiesOriginal);

		// Drop down layout style - list view with radio button
		entityDataAdapter
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

		// attaching data adapter to spinner
		entity_spinner.setAdapter(entityDataAdapter);

	}

	public void Spinner_Activity() {
		activity_spinner = (Spinner) rootView
				.findViewById(R.id.activity_spinner);
		activity_spinner.setOnItemSelectedListener(this);

		// Spinner Drop down elements -

		// Creating adapter for spinner
		activityDataAdapter = new ArrayAdapter<MyActivity>(
				rootView.getContext(), android.R.layout.simple_spinner_item,
				activitiesSelected);

		// Drop down layout style - list view with radio button
		activityDataAdapter
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

		// attaching data adapter to spinner
		activity_spinner.setAdapter(activityDataAdapter);

		// Creating adapter for spinner

	}

	public class FetchData extends AsyncTask<String, Void, Boolean> {

		@Override
		protected Boolean doInBackground(String... params) {

			String apiKey = params[0];

			HttpClient httpclient = new DefaultHttpClient();
			HttpGet httpget = new HttpGet(
					"http://winfo-androidhft.rhcloud.com/v1/entityActivity");

			try {
				Log.d("myApp", "before Response");
				HttpResponse response;
				httpget.setHeader("Authorization", apiKey);
				response = httpclient.execute(httpget);
				Log.d("myApp", "after execute");

				HttpEntity entity = response.getEntity();

				String result = null;
				result = EntityUtils.toString(entity);
				// Create a JSON object from the request response
				// Retrieve the data from the JSON object
				JSONObject jObj = new JSONObject(result);
				JSONArray jArr = jObj.getJSONArray("data");
				for (int i = 0; i < jArr.length(); i++) {
					JSONObject obj = jArr.getJSONObject(i);
					activitiesOriginal.add(new MyActivity(obj
							.getInt("idActivity"), obj
							.getString("activityHeader"), obj
							.getString("activityDescription"), obj
							.getInt("entity")));
					MyEntity newentity = new MyEntity(obj.getInt("entity"),
							obj.getString("entityName"));
					boolean entityExists = false;
					for (int j = 0; j < entitiesOriginal.size(); j++) {
						if (entitiesOriginal.get(j).getId() == newentity
								.getId()) {
							entityExists = true;
						}
					}
					if (entityExists == false) {
						entitiesOriginal.add(newentity);
					}

				}
			} catch (Exception e) {
				e.printStackTrace();
			}

			boolean ok = true;
			return ok;

		}
		
		@Override
		protected void onPostExecute(Boolean ok) {
			entity_spinner.setEnabled(true);
			activity_spinner.setEnabled(true);
		}

	}

	@Override
	public void callbackNewMainData(boolean inserted) {
		if (inserted == true) {
			textViewTime.setText(null);
			Toast.makeText(getActivity(),
					getActivity().getString(R.string.inserted),
					Toast.LENGTH_SHORT).show();
		} else {
			Toast.makeText(getActivity(),
					getActivity().getString(R.string.insertError),
					Toast.LENGTH_SHORT).show();
		}

	}

	public void setTimestampsAndPause(Date startTime, Date endTime,
			long timeWhenStopped) {
		SimpleDateFormat timestampStart = new SimpleDateFormat(
				"yyyy-MM-dd HH:mm:ss");
		startTimestampString = timestampStart.format(startTime);

		SimpleDateFormat timestampEnd = new SimpleDateFormat(
				"yyyy-MM-dd HH:mm:ss");
		endTimestampString = timestampEnd.format(endTime);

		long difference = pauseTemp;

		int days = (int) (difference / (1000 * 60 * 60 * 24));
		int hours = (int) (difference / (1000 * 60 * 60) % 24);
		int minutes = (int) (difference / (1000 * 60) % 60);
		int seconds = (int) (difference / 1000 % 60);
		int millis = (int) (difference % 1000);

		if (hours < 10) {
			if (minutes < 10) {
				if (seconds < 10) {
					pauseString = "0" + String.valueOf(hours) + ":" + "0"
							+ String.valueOf(minutes) + ":" + "0"
							+ String.valueOf(seconds);
				} else {
					pauseString = "0" + String.valueOf(hours) + ":" + "0"
							+ String.valueOf(minutes) + ":"
							+ String.valueOf(seconds);
				}

			} else {
				if (seconds < 10) {
					pauseString = "0" + String.valueOf(hours) + ":"
							+ String.valueOf(minutes) + ":" + "0"
							+ String.valueOf(seconds);
				} else {
					pauseString = "0" + String.valueOf(hours) + ":"
							+ String.valueOf(minutes) + ":"
							+ String.valueOf(seconds);
				}
			}
		} else {

			if (minutes < 10) {
				if (seconds < 10) {
					pauseString = String.valueOf(hours) + ":" + "0"
							+ String.valueOf(minutes) + ":" + "0"
							+ String.valueOf(seconds);
				} else {
					pauseString = String.valueOf(hours) + ":" + "0"
							+ String.valueOf(minutes) + ":"
							+ String.valueOf(seconds);
				}

			} else {
				if (seconds < 10) {
					pauseString = String.valueOf(hours) + ":"
							+ String.valueOf(minutes) + ":" + "0"
							+ String.valueOf(seconds);
				} else {
					pauseString = String.valueOf(hours) + ":"
							+ String.valueOf(minutes) + ":"
							+ String.valueOf(seconds);
				}
			}

		}

	}

	@Override
	public void callbackDeleteMainData(boolean deleted) {
		// TODO Auto-generated method stub

	}

	private boolean checkForValidEntries() {
		boolean isValid = true;
		
		if (stopbuttonclicked == false){
			isValid = false;
		}
		
		if (entity_spinner.getSelectedItemPosition() == 0) {
			isValid = false;
		}
		if (activity_spinner.getSelectedItemPosition() == 0) {
			isValid = false;
		}
		return isValid;
	}

	@Override
	public void callbackUpdatedMainData(boolean updated) {
		// TODO Auto-generated method stub
		
	}
	
}
