package com.example.winfo;

public class MyActivity {
	private int id;
	private String activityHeader;
	private String activityDescription;
	private MyEntity entity;
	private int entityID;

	public MyActivity(int id, String activityHeader,
			String activityDescription, MyEntity entity) {
		super();
		this.id = id;
		this.activityHeader = activityHeader;
		this.activityDescription = activityDescription;
		this.entity = entity;
	}

	public MyActivity(int id, String activityHeader,
			String activityDescription, int entityID) {
		this.id = id;
		this.activityHeader = activityHeader;
		this.activityDescription = activityDescription;
		this.entityID = entityID;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getActivityHeader() {
		return activityHeader;
	}

	public void setActivityHeader(String activityHeader) {
		this.activityHeader = activityHeader;
	}

	public String getActivityDescription() {
		return activityDescription;
	}

	public void setActivityDescription(String activityDescription) {
		this.activityDescription = activityDescription;
	}

	public MyEntity getEntity() {
		return entity;
	}

	public void setEntity(MyEntity entity) {
		this.entity = entity;
	}

	public int getEntityID() {
		return entityID;
	}

	public void setEntityID(int entityID) {
		this.entityID = entityID;
	}

	public String toString(){
		return activityHeader;
	}
	
}
